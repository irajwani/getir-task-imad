const request = require('supertest');
const app = require('../app');

describe('Fetch records request', () => {
  it('Fetched records successfully', async () => {
    const res = await request(app)
      .post('/')
      .send({
        startDate: '2015-08-09',
        endDate: '2017-09-09',
        minCount: 1200,
        maxCount: 2857,
      })
    expect(res.statusCode).toEqual(200)
    expect(res.body.code).toEqual(0)
  })
  it('Did not fetch records successfully', async () => {
    const res = await request(app)
      .post('/')
      .send({
        startDate: '2015/08-09',
        endDate: '2017-09/09',
        minCount: "483",
        maxCount: 2857,
      })
    expect(res.statusCode).toEqual(404)
  })
})