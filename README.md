# Getir backend task - Manual

A HTTP RESTful API with a singular POST endpoint exposed to fetch records, with input parameters to filter the records.

[Heroku endpoint](https://getir-task-imad.herokuapp.com)

[Public repository](https://gitlab.com/irajwani/getir-task-imad)

## Usage

Simply make a POST request in the form of a raw JSON object to the specified endpoint with the following 4 inputs:

- (String) “startDate” 
- (String) “endDate” fields 

> Should be input in a “YYYY-MM-DD” format. Records displayed will be from this specified time interval.
- (Number) “minCount”
- (Number) “maxCount” 
> For finding records for which the total count ( the sum of the “counts” array within each record) is greater than or equal to minCount and less than or equal to maxCount.

Sample JSON:

```sh
{
    "startDate": "2016-01-26", 
    "endDate": "2018-02-02", 
    "minCount": 2700, 
    "maxCount": 3000
}
```

### Response Payload
Response payload has 3 main fields.
- “code” is for status of the request. 
    - 0: Successfully retrieved 0 or more records based on input parameters
    - 1: At least one empty field
    - 2:  At least one invalid field
    - 3: Error when attempting to fetch records from cloud database
- “msg” is for description of the code, which is either "successful" or it is an explanation for an error.
- “records” includes the filtered items according to the request. This array includes items of “key”, “createdAt” and “totalCount” which is the sum of the “counts” array in the document.

Sample:  
```sh
{
    "code":0, 
    "msg":"Success", 
    "records":[
        {
            "key":"TAKwGc6Jr4i8Z487", 
            "createdAt":"2017-01-28T01:22:14.398Z", 
            "totalCount":2800
        },
        {
            "key":"NAeQ8eX7e5TEg7oH", 
            "createdAt":"2017-01-27T08:19:14.135Z", 
            "totalCount":2900
        } 
    ]
}
```


### Installation

Requires Node v14.5.0+ & npm v6.14.5+ to run

Setup:
```sh
$ git clone https://gitlab.com/irajwani/getir-task-imad.git
$ npm install
$ touch .env
$ echo DB_URI=mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true >> .env
```

Run:
```sh
$ npm start
OR
$ node ./bin/www
```

Test:
```sh
$ npm test
```

### Business logic

Query the database for it's singular collection of records. Use pipeline aggregators to: 
- transform the resultant records, such that we find the sum of the counts specified within the counts array, and declare it as a new property of the object called totalCount
- filter the infornation for records that lie within the specified time interval from startDate and endDate

Wrap this process in standard Express boilerplate, send information to client as a JSON object with a defined status, and error handling middleware.
