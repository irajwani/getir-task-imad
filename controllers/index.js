const Record = require("../models/Record");

//Helper functions
const isDateValid = (date) => {
    return /(^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$)/.test(date)
};

const isCountValid = (count) => {
    return !isNaN(parseFloat(count)) && isFinite(count) && count >= 0;
}

//Code key specified in README.md

const fetchRecords = async (req, res) => {
    let {startDate, endDate, minCount, maxCount} = req.body;

    //Check if user forgot to enter required inputs
    //Since 0 is falsy value in JS, we treat checking if the user forgot to define minCount or maxCount
    if(!startDate || !endDate || minCount == undefined || maxCount == undefined) {
        res.status(404).json({
            code: 1,
            msg: "You are missing at least one required input. Required fields: startDate, endDate, minCount, maxCount",
            records: []
        });
    }

    //Even if values are all defined, are they also valid dates and numeric values >= 0?
    else if(!isDateValid(startDate) || !isDateValid(endDate) || !isCountValid(minCount) || !isCountValid(maxCount) ) {
        res.status(404).json({
            code: 2,
            msg: "Please use YYYY-MM-DD format for startDate and endDate, and ensure minCount and maxCount are real numbers greater than or equal to 0.",
            records: []
        });
    }

    else if(minCount > maxCount) {
        res.status(404).json({
            code: 2,
            msg: "minCount must be less than or equal to maxCount",
            records: []
        });
    }
    else {
        try {
            //Use aggregate to transform the data (sum the values in counts Array and exclude _id) and filter the records given the conditions for createdAt and totalCount 
            let data = await Record.aggregate([
                {
                    $project: {
                        totalCount: { $sum: "$counts"},
                        key: "$key",
                        createdAt: "$createdAt",
                        _id: 0,
                    }
                },
                { 
                    $match: { 
                        createdAt: { $gte: new Date(startDate), $lte: new Date(endDate) }, 
                        totalCount: { $gte: minCount, $lte: maxCount} 
                    }
                }
            ]);
            res.status(200).json({
                code: 0,
                msg: "Success",
                records: data
            });
        }
        catch (error) {
            res.status(500).json({
                code: 3,
                msg: "Oops! The server has encountered an error fetching records.",
                records: []
            });
        }
    }

}

module.exports = {
    fetchRecords
}