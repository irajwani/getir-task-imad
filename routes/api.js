var express = require('express');
const { fetchRecords } = require('../controllers');
var router = express.Router();

router.post('/', fetchRecords);

module.exports = router;
